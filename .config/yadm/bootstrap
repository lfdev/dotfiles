#!/bin/env bash

YADM="yadm"
BYADM="${HOME}/bin/yadm"
LYADM="${HOME}/.local/bin/yadm"

confirm_prompt() {
    read -p "${1} (y/N)? " response
    case "$response" in
        [yY])
            # comtinue
            ;;
        *)
            exit 0
            ;;
    esac
}

is_app_installed() {
    type "${1}" &>/dev/null
}

app_not_installed() {
    printf "${1} not found, quitting...\n"
    exit 1
}

configure_arch() {
    is_app_installed sudo && true || pacman -Sy sudo
    sudo pacman -Sy git-delta
}

configure_deb() {
    is_app_installed sudo && true || apt install sudo
    is_app_installed curl && true || sudo apt install curl
    delta_url="https://github.com/dandavison/delta/releases/download/0.16.5/git-delta_0.16.5_amd64.deb"
    curl -fLo /tmp/delta.deb "${delta_url}"
    sudo apt install /tmp/delta.deb
}

configure_system() {
    system_type=$(uname -s)
    if [[ "$system_type" == "Linux" ]]; then
        is_app_installed apt && configure_deb
        is_app_installed pacman && configure_arch
    fi
}

check_yadm() {
    is_app_installed "${YADM}" && true || YADM=""
    if [[ "${YADM}" == "" ]]; then
        printf "yadm was not found in \$PATH\n"
        confirm_prompt "Try to find yadm and apply 'yadm checkout \$HOME'"
        [ -x "${BYADM}" ] && YADM="${BYADM}" || printf "${BYADM} not found or not executable\n"
        [ -x "${LYADM}" ] && YADM="${LYADM}" || printf "${LYADM} not found or not executable\n"
        [ "${YADM}" == "" ] && app_not_installed yadm || printf "${YADM} found, proceeding\n"
        "${YADM}" checkout "${HOME}"
        [[ "$SHELL" == *sh ]] && source ~/.profile || true
    fi
}

configure_yadm() {
    # Because Git submodule commands cannot operate without a work tree, they must
    # be run from within $HOME (assuming this is the root of your dotfiles)
    cd "$HOME"

    printf "Init submodules\n"
    yadm submodule update --recursive --init

    printf "Update yadm repo origin URL\n"
    yadm remote set-url origin "git@git.envs.net:lfdev/dotfiles.git"
}

configure_git() {
    read -p "Enter git name : " git_name
    read -p "Enter git email: " git_email
    [ ! -z "${git_name}" ] && git config --global "user.name"  "${git_name}"
    [ ! -z "${git_email}" ] && git config --global "user.email" "${git_email}"
}

check_yadm

is_app_installed yadm && true || app_not_installed yadm

configure_yadm

read -p "Configure system (y/N)? " response
case "$response" in
    [yY])
        configure_system
        ;;
    *)
        # continue
        ;;
esac

read -p "Configure git name/email (y/N)? " response
case "$response" in
    [yY])
        configure_git
        ;;
    *)
        # continue
        ;;
esac

printf "Logout or source ~/.profile to apply changes.\n"
